package bcas.assing.pro;

import java.util.Scanner;

public class MyBank {
	public double accBal = 0.0;

	public void deposit(double depAmt) {
		accBal = accBal + depAmt;
	}

	public void widthdraw(double widAmt) {
		accBal = accBal - widAmt;
	}

	public void bank() {

		MyBank bank = new MyBank();
		Scanner scan = new Scanner(System.in); // scanner option
		System.out.println("Welcome to Ceylon Banking System");

		// create variables
		double num, num1, num2, num3, num4 = 0;

		do { // starting do while
			System.out.print("Enter your current balance :");
			num = scan.nextDouble(); // user input
			if (num < 0) {
				System.out.println(" ***Beginning balance must be at least zero,please re-enter.***");
			}
		} while (num < 0); // end while
		bank.accBal = num;

		do { // starting do while in deposit
			System.out.print("Enter the number of deposits(0-5):");
			num1 = scan.nextDouble();// user input num1
			// if condition
			if (num1 > 5) {
				System.out.println("*** Invalid number of deposits,please re-enter.Enter the number of deposits:3***");
			}
		} while (num1 > 5); // end while

		do {
			System.out.print("Enter the number of withdrawals(0-5):");
			num2 = scan.nextDouble(); // user input num2
			// if condition
			if (num2 > 5) {
				System.out.println("*** deposit amount must be greater than zero,please re-enter ***");
			}
		} while (num2 > 5);// end while

		// include loop option in number of deposit
		for (int i = 1; i <= num1; i++) {
			System.out.print("Enter the amount of deposit " + i + " :");
			num3 = scan.nextDouble(); // user input
			bank.deposit(num3);
		}

		System.out.println("Bank balance is : " + bank.accBal);

		// starting do while
		for (int i = 1; i <= num2; i++) { // loop option in number of withdrawal
			do {
				System.out.print("Enter the amount of withdrawal " + i + " :");
				num4 = scan.nextDouble(); // user input
				if (num4 > bank.accBal) { // if condition
					System.out.println("***withdrawal amount exceeds current balance,please re-enter");
				}

			} while (num4 > bank.accBal);
			bank.widthdraw(num4);// end while

		}
		System.out.println("Bank balance is : " + bank.accBal); // print closing balance
		System.out.println("*...Thank you for using our bank system...*");
	}
}
